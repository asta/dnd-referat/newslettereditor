# Newsletter Editor

Eine kleine frontend Webanwendung um HTML newsletter für den AStA zu erstellen.

**Dieser Editor läuft ausschließlich im Browser, sprich ihr könnt euer Ergebnis nicht teilen  und Alles ist weg wenn ihr das Fenster schließt !!!!**

## Usage

Einfach auf https://asta.pages.gwdg.de/dnd-referat/newslettereditor/ gehen, dort können verschieden Newsletter Elemente hinzugefügt werden, aus denen dann html Code generiert wird. 



### Grundschritte

1. Ganz oben unter Meta den title für Deutsch und Englisch festlegen

2. Objekte hinzufügen und bearbeiten 

3. auf Preview gucken obs so aussieht wie gewünscht( das geht auch jederzeit zwischen durch)

4. Wenn alles gut ist einfach auf "copy" klicken (dadurch wird der code direkt in die Ziwschenablage kopiert)

5. Unter Thunderbird kann nun beim verfassen neuer Mails auf einfügen/HTML gegangen werden und dort der gesamte code eingefügt werden 



### Bestehende Elemente im Überblick

- Intro: Eine Textbox mit optionalem bild(wenn kein bild gewünscht ist einfach keins einfügen)

- Headline: eine zwischen überschrift hauptsächlich genutzt für "Aktuelle Meldungen"

- News: eine Box mit Bild und text daneben, wie schon immer bei dem Newsletter gilt das der text mindestens so lang sein sollte wie das Bild hoch ist da sonst die Formatierung im Arsch ist 

- Dates: die tabellarische Darstellung von Terminen(könnte natürlich auch für andere Tabellen benutzt werden jedoch ist die spalten zahl fest)

- English: im Editor nur eine kleine überschrift aber im Ergebnis dann  eine zweiter Briefkopf mit englischem title.

### Allgemein Bearbeiten

Jede Box kann oben recht mit den knöpfen verschoben bzw. gelöscht werden.



### Text bearbeiten

Auf die Hellgrauen boxen in Intro und News kann geklickt werden wodurch text areas entstehen, hier kann text eingefügt werden, sobald etwas anderes angeklickt wird wird der text in die Originale Box geschoben. Die Formatierung dieser text ist in html. 
Jedoch sind eigentlich nur sehr wenig befehle nötig um so kurze Absätze zu formatieren.

| Befehl                   | was der Macht                                                                                                                            |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------- |
| `<br>`                   | Zeilenumbruch, einfach ans ende einer Zeile schreiben                                                                                    |
| `<p>text </p>`           | der text zischen den p's wird als Paragraph behandelt, also als ein Absatz                                                               |
| `<u>underline</u>`       | für unterstrichenen Text, gilt gleich für fett(`<b>`) und kursive(`<i>`)                                                                 |
| `<a href='url'>name</a>` | Um links zu anderen Webseiten einzufügen, muss nur die URL(beginnend mit https://) angeben werden und statt name der text da stehen soll |

### Überschriften bearbeiten

Es kann einfach auf die Überschriften geklickt werden, um diese zu bearbeiten. Generell gehen dort auch die gleichen HTML befehle.



## Bilder hinzufügen

Um Bilder hinzuzufügen kann einfach auf die dunkelgraue Box(Into) oder auf die Rote Box (news ) geklickt werden, dann erscheint ein button mit durchsuchen, da kann einfach das Bild vom Computer aus hochgeladen werden.

Wenn das bild später geändert werden soll kann einfach wieder auf die box geklickt werden.



### Dates hinzufügen

Jeder Dates block hat einen Titel und am rechten rand einen Knopf um Neue Daten hinzu zu fügen. Dadurch entsteht eine neue Reihe in der Termine mit ort und uhrzeit eingetragen werden können. 

Diese Termine können am rechten rand verschoben und gelöscht werden.

 
