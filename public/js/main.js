
// HTML code for the different parts that can be added to the tool bar
var moving_control = "<div style='width:100%;'class='moving_control'><button onclick='move(this.parentNode.parentNode,-1)'>&uarr;</button><button onclick='move(this.parentNode.parentNode,2)'>&darr;</button><button onclick='drop(this.parentNode.parentNode)'> x </button></div>";

var newsBox_code = `
	<div class='newsBox'>`+ moving_control + `
		<div class='imgBox' onclick='addImg(this)'></div>
			<div class='textBox'>
				<h3 id='headline' onclick='editHeadline(this)'>news</h3>
			<div class='newsText' onclick='editBox(this)'></div>
		</div>
	</div>`;

var dateTable_code = `<div class='dateTable'>` + moving_control + ` 
		Title : <input type="text" id="table_Title"><button onclick="addDate(this.parentNode)">new Date</button>
			<table id="timeTable" style="width:100%; border:1px solid black;" >
		</table>
		</div>
	`;

var headLine_code = "<div class='headLineBox'>" + moving_control + "<h2 id='headline' onclick='editHeadline(this)'>section</h2></div>";

var intro_code = "<div class='introBox'>" + moving_control + "<div class='introText' onclick='editBox(this)'></div><div class='IntroImgBox' onclick='addImgXL(this)'></div></div>"



// resolver for the buttons
function addNewsBox() {
	document.getElementById("newsLetter").innerHTML += newsBox_code;
}

function addHeadlineBox() {
	document.getElementById("newsLetter").innerHTML += headLine_code;
}

function addIntroBox() {
	document.getElementById("newsLetter").innerHTML += intro_code;
}

function addEnglischBreak() {
	document.getElementById("newsLetter").innerHTML += "<div class='langBreak'>" + moving_control + "<h3>english</h3></div>";
}
function addDateBox() {
	document.getElementById("newsLetter").innerHTML += dateTable_code;
}

// Function to style the html links
function linkEdit(text) {
	newLink = ' <a style="text-decoration: none; color: #a71680;" ';
	erg = "";
	text.split("<a ").forEach(function (te) {
		erg += te + newLink;

	});
	return erg.slice(0, -newLink.length)
}


// Iterating over all elements of the newsletter and create a html code based on the temp.js file
function exportNewsLetter() {
	var nL = document.getElementById("newsLetter");
	var erg = get_html_head(document.getElementById("name_de").value);
	var vorsitz = document.getElementById("vorsitz").value;
	var langErg=["",""];
	var langMen=["<ul>","<ul>"];
	var step=1;
	//langErg[0] ;
	//langErg[1] += get_asta_head(document.getElementById("name_en").value, "en");
	var langStep = 0;
	Array.from(nL.children).forEach(function (val) {
		if (val.className == "newsBox") {
			step++;
			var col = "#fcf5fa";
			if (step % 2 == 0) {
				col = "white";
			}
			langErg[langStep] += get_news_Box(val.querySelector("#headline").innerHTML, linkEdit(val.querySelector(".newsText").innerHTML), val.querySelector('.imgBox').style.backgroundImage.slice(4, -1).replace(/"/g, ""), col);
			langMen[langStep] +=`<li><a href="#`+val.querySelector("#headline").innerHTML+`">`+val.querySelector("#headline").innerHTML+`</a></li>`
		} else if (val.className == "headLineBox") {
			langErg[langStep] += get_headline(val.querySelector("#headline").innerHTML);
		} else if (val.className == "introBox") {
			langErg[langStep] += get_intro_box(linkEdit(val.querySelector(".introText").innerHTML), val.querySelector('.IntroImgBox').style.backgroundImage.slice(4, -1).replace(/"/g, ""));
		} else if (val.className == "langBreak") {
			langErg[langStep] += get_food_note(vorsitz);
			langStep++;
		} else if (val.className == "dateTable") {
			langErg[langStep]  += createDateBlock(val);
		}
	});
	erg+=get_asta_head(document.getElementById("name_de").value, "de")+linkEdit(langMen[0])+"</ul>"+langErg[0]+get_asta_head(document.getElementById("name_en").value, "en")+linkEdit(langMen[1])+"</ul>"+langErg[1]+ get_food_note(vorsitz);
	return erg;
}
// copy html code to clipboard 
function copyNewsLetter() {
	erg = exportNewsLetter();
	navigator.clipboard.writeText(erg);
}

// show preview in a new browser tab
function viewNewsLetter() {
	erg = exportNewsLetter();
	var w = window.open("");
	w.document.write(erg);


}

// Edit and save different html objects by onfocus and onfocusout
function editBox(obj) {
	var content = obj.innerHTML;
	if (content.includes("<textarea") == false) {
		obj.innerHTML = "<textarea id='boxEditor' onfocusout='saveBox(this)' style='width:100%;'autofocus >" + content + "</textarea>";
		document.getElementById('boxEditor').focus();
	}
}
function saveBox(area) {
	area.parentNode.innerHTML = area.value;
}

function editHeadline(obj) {
	var content = obj.innerHTML;
	if (content.includes("<input") == false) {
		obj.innerHTML = "<input  id='hEditor' onfocusout='saveHeadline(this)'value='" + content + "'>";
		document.getElementById('hEditor').focus();
	}
}
function saveHeadline(area) {
	area.parentNode.innerHTML = area.value;
}


// add img upload dialog to img box and clean all other upload dialogs to avoid B.S.
function addImg(imgBox) {
	var content = imgBox.innerHTML;
	if (content.includes("<input") == false) {
		document.querySelectorAll(".imgBox").forEach(e => {
			e.innerHTML = "";
		});
		document.querySelectorAll(".IntroImgBox").forEach(e => {
			e.innerHTML = "";
		});


		imgBox.innerHTML = ' <input type="file" id="image_uploads" name="image_uploads" accept=".jpg, .jpeg, .png">'
		document.getElementById('image_uploads').addEventListener('change', updateImageDisplay);
	}
}

// set new uploaded image as background image and clear imgBox
function updateImageDisplay() {
	var imgBox = document.getElementById("image_uploads").parentNode;
	var img = new Image();
	var reader = new FileReader();
	var imgURL = document.getElementById('image_uploads').files[0];
	var imgLink = URL.createObjectURL(imgURL);
	img.src = imgLink;
	img.onload = function () {
		var can = document.createElement('canvas');
		can.width = 400;
		can.height = 400;
		var ctx = can.getContext('2d');
		ctx.drawImage(img, 0, 0, 400, 400);
		imgBox.style.backgroundImage = "Url('" + can.toDataURL("image/png") + "')";
		imgBox.style.backgroundSize = "cover";
		imgBox.innerHTML = "";

	}
}
//same as above for bigger Images
function addImgXL(imgBox) {
	var content = imgBox.innerHTML;
	if (content.includes("<input") == false) {
		document.querySelectorAll(".imgBox").forEach(e => {
			e.innerHTML = "";
		});
		document.querySelectorAll(".IntroImgBox").forEach(e => {
			e.innerHTML = "";
		});

		imgBox.innerHTML = '<input type="file" id="image_uploads" name="image_uploads" accept=".jpg, .jpeg, .png">'
		document.getElementById('image_uploads').addEventListener('change', updateImageDisplayXL);
	}
}
function updateImageDisplayXL() {
	var imgBox = document.getElementById("image_uploads").parentNode;
	var img = new Image();
	var reader = new FileReader();
	var imgURL = document.getElementById('image_uploads').files[0];
	var imgLink = URL.createObjectURL(imgURL);
	img.src = imgLink;
	img.onload = function () {
		var can = document.createElement('canvas');
		can.width = 800;
		can.height = 800;
		var ctx = can.getContext('2d');
		ctx.drawImage(img, 0, 0, 800, 800);
		imgBox.style.backgroundImage = "Url('" + can.toDataURL("image/png") + "')";
		imgBox.style.backgroundSize = "cover";
		imgBox.innerHTML = "";

	}

}
// move the different parts of the newsletter up or down 
// Works as well for the rows in the Dates table
function move(child, ind) {
	var parent = child.parentNode;
	var index = Array.prototype.indexOf.call(parent.children, child);
	console.log(parent.children.length);
	if ((index > 0 && ind == -1) || (index < parent.children.length && ind == 2)) {
		parent.insertBefore(child, parent.children[index + ind]);
	}
}
// remove part of the newsletter
// Works as well for the rows in the Dates table
function drop(node) {
	node.parentNode.removeChild(node);
}



// create empty row in a date Table
function addDate(node) {
	node.querySelector("#timeTable").innerHTML += `<tr>
						<td style="vertical-align: baseline; width:140px;" onclick='editHeadline(this)' ></td>
                    				<td style="vertical-align: baseline; width:220px;" onclick='editHeadline(this)'></td>
                    				<td style="vertical-align: baseline; width:300px;" onclick='editHeadline(this)'></td>
       		    				<td id="control"><button onclick='move(this.parentNode.parentNode,-1)'>&uarr;</button><button onclick='move(this.parentNode.parentNode,2)'>&darr;</button><button onclick='drop(this.parentNode.parentNode.parentNode)'> x </button></td> 
				       </tr>`;

}

